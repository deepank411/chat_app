import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceService {
  SharedPreferences _prefs;

  Future<bool> getSharedPreferencesInstance() async {
    _prefs = await SharedPreferences.getInstance().catchError((e) {
      print("shared prefrences error : $e");
      return false;
    });
    return true;
  }

  Future setAccessToken(String accessToken) async {
    await _prefs.setString('accessToken', accessToken);
  }

  Future setSeenLaunchScreen(bool val) async {
    await _prefs.setBool('seenLaunchScreen', val);
  }

  Future setLoggedInUserData(String userData) async {
    await _prefs.setString('userData', userData);
  }

  Future clearPrefs() async {
    await _prefs.clear();
  }

  Future<String> get accessToken async => _prefs.getString('accessToken');

  Future<String> get userData async => _prefs.getString('userData');

  Future<bool> get seenLaunchScreen async => _prefs.getBool('seenLaunchScreen');

  Future removeSeenLaunceScreen() async {
    await _prefs.remove('seenLaunchScreen');
  }
}

SharedPreferenceService sharedPreferenceService = SharedPreferenceService();
