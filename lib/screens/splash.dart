import 'dart:async';
import 'dart:convert' as JSON;
import 'package:flutter/material.dart';

import 'package:flipo/services/shared_preferences_service.dart';

import 'package:flipo/screens/login.dart';
import 'package:flipo/screens/dashboard.dart';
import 'package:flipo/screens/launch.dart';

class Splash extends StatefulWidget {
  @override
  SplashState createState() => SplashState();
}

class SplashState extends State<Splash> {
  Future checkFirstSeen() async {
    await sharedPreferenceService.getSharedPreferencesInstance();
    String _token = await sharedPreferenceService.accessToken;
    bool _seenLaunchScreen =
        await sharedPreferenceService.seenLaunchScreen ?? false;

    if (_seenLaunchScreen) {
      if (_token == null || _token == '') {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Dashboard(),
          ),
        );
      } else {
        final Map<String, dynamic> _userData =
            JSON.jsonDecode(await sharedPreferenceService.userData);
        print(['splash:', '_userData', _userData, _token]);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) =>
                _userData['id'] != null ? Dashboard() : LoginPage(),
          ),
        );
      }
    } else {
      await sharedPreferenceService.setSeenLaunchScreen(true);
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Launch(),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(milliseconds: 500), () {
      checkFirstSeen();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
