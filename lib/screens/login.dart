import 'dart:convert' as JSON;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:transparent_image/transparent_image.dart';

import 'package:flipo/utils/toast.dart';
import 'package:flipo/utils/colors.dart';
import 'package:flipo/utils/network/constants/endpoints.dart';
import 'package:flipo/utils/constants.dart' as Constants;

import 'package:flipo/services/shared_preferences_service.dart';

import 'package:flipo/screens/dashboard.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoggingIn = false;

  void _getTokenAfterLogin(apiObject) async {
    final http.Response response = await http.post(
      Endpoints.LOGIN,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: JSON.jsonEncode(apiObject),
    );
    if (response.statusCode == 200) {
      final profile = JSON.jsonDecode(response.body);
      if (profile['status'] == 200) {
        await sharedPreferenceService.getSharedPreferencesInstance();
        await sharedPreferenceService
            .setAccessToken(profile['data']['accessToken']);
        await sharedPreferenceService
            .setLoggedInUserData(JSON.jsonEncode(profile['data']['user']));
        setState(() {
          _isLoggingIn = true;
        });
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => Dashboard(),
          ),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Welcome',
                    style: TextStyle(
                      fontSize: 36.0,
                      color: AppColors.cSmoke,
                    ),
                  ),
                  SizedBox(height: 40),
                  FadeInImage.memoryNetwork(
                    placeholder: kTransparentImage,
                    image: '${Constants.IMAGE_URL}/assets/login/auth-0.png',
                    imageScale: 1.6,
                  ),
                  SizedBox(height: 30),
                  Text(
                    'Join our community.',
                    style: TextStyle(
                      fontSize: 16,
                      color: AppColors.cSmoke,
                    ),
                  ),
                  SizedBox(height: 10),
                  Text(
                    'Get feedback on your image',
                    style: TextStyle(
                      fontSize: 16,
                      color: AppColors.cSmoke,
                    ),
                  ),
                  SizedBox(height: 60),
                  _isLoggingIn
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : Column(
                          children: <Widget>[
                            SizedBox(
                              width: 200,
                              height: 40,
                              child: RaisedButton(
                                child: Text('Login with Facebook'),
                                color: AppColors.cFacebook,
                                textColor: Colors.white,
                                onPressed: () async {
                                  setState(() {
                                    _isLoggingIn = true;
                                  });
                                  FacebookLoginResult facebookLoginResult =
                                      await _handleFacebookLogin();
                                  var fbResponse;

                                  switch (facebookLoginResult.status) {
                                    case FacebookLoginStatus.loggedIn:
                                      final token =
                                          facebookLoginResult.accessToken.token;
                                      final graphResponse = await http.get(
                                          'https://graph.facebook.com/v2.12/me?fields=name,email&access_token=$token');

                                      final profile =
                                          JSON.jsonDecode(graphResponse.body);
                                      fbResponse = {
                                        'userProfile': {
                                          'fbUserId': profile['id'],
                                          'fullName': profile['name'],
                                          'email': profile['email'],
                                          'coverPicture':
                                              'https://graph.facebook.com/${profile['id']}/picture?width=1080',
                                        },
                                        'isLoggedIn': true,
                                        'provider': 'FACEBOOK',
                                      };
                                      break;

                                    case FacebookLoginStatus.cancelledByUser:
                                      fbResponse = {
                                        'error': 'Login cancelled by user.',
                                        'isLoggedIn': false,
                                        'provider': 'FACEBOOK',
                                      };
                                      setState(() {
                                        _isLoggingIn = false;
                                      });
                                      showToast('Login cancelled');
                                      break;

                                    case FacebookLoginStatus.error:
                                      fbResponse = {
                                        'error': 'Something went wrong.',
                                        'isLoggedIn': false,
                                        'provider': 'FACEBOOK',
                                      };
                                      setState(() {
                                        _isLoggingIn = false;
                                      });
                                      showToast('Something went wrong');
                                      break;
                                  }
                                  _getTokenAfterLogin(
                                      fbResponse['userProfile']);
                                  return null;
                                },
                              ),
                            ),
                            SizedBox(height: 20),
                            SizedBox(
                              width: 200,
                              height: 40,
                              child: RaisedButton(
                                child: Text('Login with Google'),
                                color: Colors.white,
                                textColor: AppColors.cStone,
                                onPressed: () async {
                                  try {
                                    setState(() {
                                      _isLoggingIn = true;
                                    });
                                    GoogleSignInAccount googleSignInAccount =
                                        await _handleGoogleSignIn();
                                    var googleResponse = {
                                      'isLoggedIn': true,
                                      'provider': 'GOOGLE',
                                      'userProfile': {
                                        'googleUserId': googleSignInAccount.id,
                                        'fullName':
                                            googleSignInAccount.displayName,
                                        'email': googleSignInAccount.email,
                                        'coverPicture':
                                            googleSignInAccount.photoUrl,
                                      },
                                    };
                                    _getTokenAfterLogin(
                                        googleResponse['userProfile']);
                                  } catch (error) {
                                    print(error);
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ],
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<FacebookLoginResult> _handleFacebookLogin() async {
    FacebookLogin facebookLogin = FacebookLogin();
    facebookLogin.loginBehavior = FacebookLoginBehavior.webViewOnly;
    FacebookLoginResult facebookLoginResult =
        await facebookLogin.logIn(['email']);
    return facebookLoginResult;
  }

  Future<GoogleSignInAccount> _handleGoogleSignIn() async {
    GoogleSignIn googleSignIn = GoogleSignIn(scopes: ['email', 'profile']);
    GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    return googleSignInAccount;
  }
}
