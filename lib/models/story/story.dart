class Story {
  String id;
  String userId;
  List<Map<String, dynamic>> images;
  int status;
  DateTime createdAt;
  DateTime updatedAt;

  Story({
    this.id,
    this.userId,
    this.images,
    this.status,
    this.createdAt,
    this.updatedAt,
  });

  factory Story.fromMap(Map<String, dynamic> json) => Story(
        id: json["id"],
        userId: json["userId"],
        images: json["images"],
        status: json["status"],
        createdAt: json["createdAt"],
        updatedAt: json["updatedAt"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "userId": userId,
        "images": images,
        "status": status,
        "createdAt": createdAt,
        "updatedAt": updatedAt,
      };
}
