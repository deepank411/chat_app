import 'package:flipo/models/story/story.dart';

class StoryList {
  final List<Story> stories;

  StoryList({
    this.stories,
  });

  factory StoryList.fromJson(List<dynamic> json) {
    List<Story> stories = List<Story>();
    stories = json.map((story) => Story.fromMap(story)).toList();

    return StoryList(
      stories: stories,
    );
  }
}
