import 'package:flutter/material.dart';

class AppColors {
  AppColors._(); // this basically makes it so you can instantiate this class

  static const Map<int, Color> orange = const <int, Color>{
    50: Color(0xFFffece0),
    100: Color(0xFFffd0b3),
    200: Color(0xFFffb180),
    300: Color(0xFFff914d),
    400: Color(0xFFff7a26),
    500: Color(0xFFff6200),
    600: Color(0xFFff5a00),
    700: Color(0xFFff5000),
    800: Color(0xFFff4600),
    900: Color(0xFFff3400),
  };

  static const Map<int, Color> blue = const <int, Color>{
    50: Color(0xFFE7E8FB),
    100: Color(0xFFC2C5F5),
    200: Color(0xFF9A9FEF),
    300: Color(0xFF7279E9),
    400: Color(0xFF535CE4),
    500: Color(0xFF353FDF),
    600: Color(0xFF3039DB),
    700: Color(0xFF2831D7),
    800: Color(0xFF2229D2),
    900: Color(0xFF161BCA),
  };

  static const cBgGray = Color(0xFFF0F3FF);
  static const cText = Color(0xFF8B96BF);

  static const cGraphite = Color(0xFF1D2337);
  static const cFacebook = Color(0xFF3B5998);
  static const cStone = Color(0xFF494949);
  static const cSmoke = Color(0xFF787E8F);
  static const cCloud = Color(0xFFE5E6F6);
  static const cGoose = Color(0xFFFAFAFA);
  static const cGray = Color(0xFFFBFBFB);
  static const cSilver = Color(0xFFFFF8E7);
}
