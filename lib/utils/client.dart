import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'package:flipo/services/shared_preferences_service.dart';

class Config {
  static final HttpLink httpLink = HttpLink(
    uri: 'http://localhost:8080/graphql',
  );
  static final AuthLink authLink =
      AuthLink(getToken: () async => await sharedPreferenceService.accessToken);
  static final WebSocketLink websocketLink = WebSocketLink(
    url: 'ws://localhost:8080/subscriptions',
    config: SocketClientConfig(
      autoReconnect: true,
      inactivityTimeout: Duration(seconds: 30),
    ),
  );
  static final Link link =
      authLink.concat(httpLink as Link).concat(websocketLink);
  static ValueNotifier<GraphQLClient> initailizeClient() {
    ValueNotifier<GraphQLClient> client = ValueNotifier(
      GraphQLClient(
        cache: OptimisticCache(dataIdFromObject: typenameDataIdFromObject),
        link: link,
      ),
    );
    return client;
  }
}
