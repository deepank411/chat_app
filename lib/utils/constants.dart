library constants;

const String IMAGE_URL = 'https://goflat.imgix.net';

const interactionTypes = {
  'STATUS_LIKE': 101,
  'STATUS_SWIPE': 102,
};
