import 'package:flutter/material.dart';

import 'colors.dart';

final ThemeData themeData = new ThemeData(
  brightness: Brightness.light,
  primarySwatch: MaterialColor(AppColors.orange[500].value, AppColors.orange),
  primaryColor: AppColors.orange[500],
  primaryColorBrightness: Brightness.light,
  accentColor: AppColors.blue[500],
  accentColorBrightness: Brightness.light,
  canvasColor: Color(0xFFFAFAFA),
  fontFamily: 'JosefinSans',
  buttonColor: AppColors.orange[500],
);

final ThemeData themeDataDark = ThemeData(
  brightness: Brightness.dark,
  primaryColor: AppColors.orange[500],
  primaryColorBrightness: Brightness.dark,
  accentColor: AppColors.blue[500],
  accentColorBrightness: Brightness.dark,
  canvasColor: Color(0xFF1D2337),
  fontFamily: 'JosefinSans',
  buttonColor: AppColors.orange[500],
);
