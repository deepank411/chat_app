class Endpoints {
  Endpoints._();

  // base url
  static const String BASE_URL = "http://localhost:8080";

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  // auth endpoints
  static const String LOGIN = BASE_URL + "/api/v1/login";

  // user endpoints
  static const String UPDATE_USER = BASE_URL + '/v1/user';

  // generic
  static const String CREATE_MONGO_ID =
      BASE_URL + '/api/v1/create-new-mongo-id';
}
