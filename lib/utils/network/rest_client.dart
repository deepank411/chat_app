import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flipo/services/shared_preferences_service.dart';
import 'package:http/http.dart' as http;

import 'exceptions/network_exceptions.dart';

class RestClient {
  // instantiate json decoder for json serialization
  final JsonDecoder _decoder = JsonDecoder();

  // Get:-----------------------------------------------------------
  Future<dynamic> get(String url) async {
    await sharedPreferenceService.getSharedPreferencesInstance();
    final String _token = await sharedPreferenceService.accessToken;
    return http.get(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader:
            (_token != null && _token != '') ? 'Bearer $_token' : null,
      },
    ).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw NetworkException(
            message: "Error fetching data from server", statusCode: statusCode);
      }
      return _decoder.convert(res);
    });
  }

  // Post:-----------------------------------------------------------
  Future<dynamic> post(String url, {Map headers, body, encoding}) async {
    await sharedPreferenceService.getSharedPreferencesInstance();
    final String _token = await sharedPreferenceService.accessToken;
    return http
        .post(
      url,
      body: body,
      headers: <String, String>{
        ...headers,
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader:
            (_token != null && _token != '') ? 'Bearer $_token' : null,
      },
      encoding: encoding,
    )
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw NetworkException(
            message: "Error fetching data from server", statusCode: statusCode);
      }
      return _decoder.convert(res);
    });
  }

  // Put:-----------------------------------------------------------
  Future<dynamic> put(String url, {Map headers, body, encoding}) async {
    await sharedPreferenceService.getSharedPreferencesInstance();
    final String _token = await sharedPreferenceService.accessToken;
    return http
        .put(
      url,
      body: body,
      headers: <String, String>{
        ...headers,
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader:
            (_token != null && _token != '') ? 'Bearer $_token' : null,
      },
      encoding: encoding,
    )
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw NetworkException(
            message: "Error fetching data from server", statusCode: statusCode);
      }
      return _decoder.convert(res);
    });
  }
}
