import 'dart:async';

import 'package:flipo/utils/network/constants/endpoints.dart';
import 'package:flipo/utils/network/exceptions/network_exceptions.dart';
import 'package:flipo/utils/network/rest_client.dart';
import 'package:flipo/models/story/story_list.dart';

class StoryApi {
  final RestClient _restClient;

  StoryApi(this._restClient);

  Future<StoryList> getStories() {
    return _restClient
        .get(Endpoints.GET_STORIES)
        .then((dynamic res) => StoryList.fromJson(res))
        .catchError((error) => throw NetworkException(message: error));
  }

  Future<StoryList> getMyStories() {
    return _restClient
        .get(Endpoints.GET_MY_STORIES)
        .then((dynamic res) => StoryList.fromJson(res))
        .catchError((error) => throw NetworkException(message: error));
  }
}
