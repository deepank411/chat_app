import 'dart:async';
import 'dart:convert' as JSON;
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flipo/utils/colors.dart';
import 'package:flipo/utils/constants.dart' as Constants;
import 'package:flipo/utils/network/constants/endpoints.dart';

import 'package:flipo/services/shared_preferences_service.dart';

class ViewPhotos extends StatefulWidget {
  ViewPhotos({Key key, this.storyData}) : super(key: key);

  final Map storyData;
  @override
  _ViewPhotosState createState() => _ViewPhotosState();
}

class _ViewPhotosState extends State<ViewPhotos> {
  bool loading = false;
  List stats = [];

  Future getStoryStats(String storyId) async {
    await sharedPreferenceService.getSharedPreferencesInstance();
    final String _token = await sharedPreferenceService.accessToken;
    setState(() {
      loading = true;
    });
    final http.Response res = await http.get(
      '${Endpoints.GET_INTERACTION_STATS}?storyId=$storyId',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        HttpHeaders.authorizationHeader: 'Bearer $_token',
      },
    );
    final storyStats = JSON.jsonDecode(res.body);
    if (storyStats['status'] == 200) {
      stats = storyStats['data'];
    }
    loading = false;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    this.getStoryStats(widget.storyData['id']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Icon(
                        Icons.chevron_left,
                        size: 50,
                        color: AppColors.cSmoke,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(
                      vertical: 20,
                      horizontal: 15,
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: <Widget>[
                                Hero(
                                  tag: widget.storyData['images'][0]['id'],
                                  child: Image(
                                    image: NetworkImage(
                                        '${Constants.IMAGE_URL}${widget.storyData['images'][0]['url']}?w=600&h=600'),
                                    fit: BoxFit.cover,
                                    key: UniqueKey(),
                                    height: 150,
                                    width: 150,
                                  ),
                                ),
                                SizedBox(height: 50),
                                stats.length == 0
                                    ? Container()
                                    : loading
                                        ? CircularProgressIndicator()
                                        : Text(
                                            "Likes: ${stats.indexWhere((note) => note['imageId'] == widget.storyData['images'][0]['id']) >= 0 ? stats[stats.indexWhere((note) => note['imageId'] == widget.storyData['images'][0]['id'])]['count'] : 0}",
                                          ),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Hero(
                                  tag: widget.storyData['images'][1]['id'],
                                  child: Image(
                                    image: NetworkImage(
                                        '${Constants.IMAGE_URL}${widget.storyData['images'][1]['url']}?w=600&h=600'),
                                    fit: BoxFit.cover,
                                    key: UniqueKey(),
                                    height: 150,
                                    width: 150,
                                  ),
                                ),
                                SizedBox(height: 50),
                                stats.length == 0
                                    ? Container()
                                    : loading
                                        ? CircularProgressIndicator()
                                        : Text(
                                            "Likes: ${stats.indexWhere((note) => note['imageId'] == widget.storyData['images'][1]['id']) >= 0 ? stats[stats.indexWhere((note) => note['imageId'] == widget.storyData['images'][1]['id'])]['count'] : 0}",
                                          ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(height: 50),
                        stats.length == 0
                            ? Text('No analytics found')
                            : loading
                                ? CircularProgressIndicator()
                                : Text(
                                    "No photo liked: ${stats.indexWhere((note) => note['type'] == Constants.interactionTypes['STATUS_SWIPE']) >= 0 ? stats[stats.indexWhere((note) => note['type'] == Constants.interactionTypes['STATUS_SWIPE'])]['count'] : 0}",
                                  ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
