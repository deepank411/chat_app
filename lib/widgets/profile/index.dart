import 'dart:convert' as JSON;
import 'package:flipo/widgets/profile/user_info.dart';
import 'package:flutter/material.dart';

import 'package:flipo/services/shared_preferences_service.dart';

import 'package:flipo/utils/colors.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  dynamic _parsedUserData;

  @override
  void initState() {
    super.initState();
    _getAccessToken();
  }

  _getAccessToken() async {
    await sharedPreferenceService.getSharedPreferencesInstance();
    String _userData = await sharedPreferenceService.userData;
    if (_userData != null && _userData != '') {
      setState(() {
        _parsedUserData = JSON.jsonDecode(_userData);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.cBgGray,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _parsedUserData != null
              ? UserInfo(
                  userData: _parsedUserData,
                )
              : CircularProgressIndicator(),
        ],
      ),
    );
  }
}
