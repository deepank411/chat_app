import 'package:flutter/material.dart';

import 'package:flipo/services/shared_preferences_service.dart';

import 'package:flipo/utils/colors.dart';

import 'package:flipo/screens/login.dart';

class UserInfo extends StatelessWidget {
  const UserInfo({
    Key key,
    this.userData,
  }) : super(key: key);

  final Map userData;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          width: double.infinity,
          height: 180,
          margin: EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(8),
              bottomRight: Radius.circular(8),
            ),
            color: AppColors.blue[500],
          ),
          padding: EdgeInsets.fromLTRB(130, 0, 0, 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                userData['fullName'] ?? '-',
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                softWrap: false,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 10),
              GestureDetector(
                onTap: () async {
                  await sharedPreferenceService.getSharedPreferencesInstance();
                  await sharedPreferenceService.clearPrefs();
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LoginPage(),
                    ),
                  );
                },
                child: Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Text(
                    'Logout',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 0,
          left: 20,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              boxShadow: [
                BoxShadow(
                  color: Colors.white,
                  offset: Offset(-5, -5),
                ),
              ],
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image(
                image: NetworkImage(
                  userData['coverPicture'] ?? 'http://placehold.it/100x150',
                ),
                fit: BoxFit.cover,
                key: UniqueKey(),
                height: 150,
                width: 100,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
